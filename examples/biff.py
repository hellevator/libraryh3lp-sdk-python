#!/usr/bin/env python

# biff.py
# -------
# Poll 3mail for new messages and send notifications to the terminal.
# See https://www.freebsd.org/cgi/man.cgi?query=biff
#
# Usage: ./biff.py &

from datetime import datetime

import lh3.api
import pytz
import time

utc=pytz.UTC

def parse_isoformat(timestring):
    """
    For whatever reason, Python doesn't include a built-in to parse ISO
    timestamps.
    """
    try:
        if timestring.endswith("Z"):
            timestring = timestring[:-1]
        return datetime.strptime(timestring, "%Y-%m-%dT%H:%M:%S").replace(tzinfo = utc)
    except:
        head, _sep, tail = timestring.rpartition(":")
        return datetime.strptime(head+tail, "%Y-%m-%dT%H:%M:%S%z").replace(tzinfo = utc)

# Fetch all Inbox email threads, including their read status and the time of
# the last message.
def inbox_threads(client, mailboxes):
    search = {
        "mailbox": mailboxes,
        "tag": ["Inbox"]
    }
    threads = client.api().get('v4', '/emails/_search', params = search)
    for t in threads:
        t['date'] = parse_isoformat(t['date'])
    return threads

# Return only those threads that have arrived since we last checked.
def is_new_thread(prev_threads, threads):
    prev = {t['thread_id']: t['date'].replace(tzinfo = utc) for t in prev_threads}
    return [t for t in threads if t['date'] > prev.get(t['thread_id'], datetime.min.replace(tzinfo = utc))]

# Summarize the new activity on the console.
def notify_user(threads):
    print('New messages in {} threads'.format(len(threads)))
    for t in threads:
        print('{subject} from {sender}'.format(**t))

def main():
    client = lh3.api.Client()
    mailboxes = [m[u'name'] for m in client.api().get('v4', '/emails/mailboxes')]

    prev_threads = inbox_threads(client, mailboxes)
    print({t['thread_id']: t['date'] for t in prev_threads})
    while True:
        time.sleep(300)
        client = lh3.api.Client()
        threads = inbox_threads(client, mailboxes)
        new_threads = is_new_thread(prev_threads, threads)
        notify_user(new_threads)
        prev_threads = threads

if __name__ == '__main__':
    main()
