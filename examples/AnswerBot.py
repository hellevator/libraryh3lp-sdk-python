#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import logging
import getpass
from optparse import OptionParser

"""
    Adapted from echo bot example in Slixmpp.
"""

import requests
import slixmpp
import ssl
import xml.etree.ElementTree as ET

class AnswerBot(slixmpp.ClientXMPP):

    """
    A simple LibraryH3lp bot that will respond to basic commands with
    predetermined responses. No fancy AI.

    To keep track of conversations, you should create a LibraryH3lp queue
    and assign *only* the bot as an operator on the queue.  Then the bot will
    answer all incoming chats from patrons on that queue.  When a patron
    requests a transfer to a human, the bot can transfer them to a different
    queue staffed by humans.
    """

    commands = {
        'hours': ('8am - 5pm weekdays. 9am - 5pm Saturday. Closed Sunday.', 'Library operating hours'),
        'directions': ('At the corner of Main St. and Martin Luther King Blvd.', 'Library location')
    }

    # Keep track of ongoing conversations with guests.
    # Key is a bare guest JID. Value is a list containing conversation ID
    # and an authorization token for transfers.
    conversations = {}

    def __init__(self, username, domain, resource, password, log, queue = None):
        slixmpp.ClientXMPP.__init__(self, '{}@{}/{}'.format(username, domain, resource), password)

        self.domain         = domain
        self.transfer_queue = queue
        self.log            = log

        # The session_start event will be triggered when
        # the bot establishes its connection with the server
        # and the XML streams are ready for use. We want to
        # listen for this event so that we we can initialize
        # our roster.
        self.add_event_handler('session_start', self.start)

        # The message event is triggered whenever a message
        # stanza is received. Be aware that that includes
        # MUC messages and error messages.
        self.add_event_handler('chatstate_active', self.guest_message)
        self.add_event_handler('message', self.message)
        self.add_event_handler('roster_update', self.roster_update)

    def start(self, event):
        """
        Process the session_start event.

        Typical actions for the session_start event are
        requesting the roster and broadcasting an initial
        presence stanza.

        Arguments:
            event -- An empty dictionary. The session_start
                     event does not provide any additional
                     data.
        """
        self.send_presence()
        self.get_roster()

    def roster_update(self, iq):
        """
        Keep track of which guests we've chatted with.
        """
        root = ET.fromstring(iq.__str__())
        for query in root:
            for item in query:
                if item.get('subscription') == 'both':
                    guest_jid = item.get('jid')
                    self.log.debug('roster update: {}'.format(guest_jid))
                    self['xep_0054'].get_vcard(guest_jid, callback = self.handle_vcard)

    def message(self, msg):
        """
        Process incoming message stanzas. Be aware that this also
        includes MUC messages and error messages. It is usually
        a good idea to check the message's type before processing
        or sending replies.

        Arguments:
            msg -- The received message stanza. See the documentation
                   for stanza objects and the Message stanza to see
                   how it may be used.
        """
        self.log.debug("message: {}".format(msg["body"]))

    def guest_message(self, msg):
        """
        Process incoming message stanzas. Be aware that this also
        includes MUC messages and error messages. It is usually
        a good idea to check the messages's type before processing
        or sending replies.

        Arguments:
            msg -- The received message stanza. See the documentation
                   for stanza objects and the Message stanza to see
                   how it may be used.
        """
        self.log.debug('guest message: {}'.format(msg['body']))

        if msg['type'] in ('chat', 'normal') and msg['body']:
            reply = self.process_command(msg)
            if reply:
                msg.reply(reply).send()

    def handle_vcard(self, iq):
        """
        VCards for guests include a chat management URL which contains a
        conversation ID and token.  The conversation ID and token can later be
        used for transfers.
        """
        vcard = ET.fromstring(iq.__str__())
        chat_management = getattr(vcard.find('.//{vcard-temp}X-CHAT-MANAGEMENT'), 'text', None)
        if not chat_management:
            return; # Not a guest

        self.log.debug('Received guest vcard: {}'.format(vcard))

        guest_jid = iq['from']
        chat_id = chat_management.split('/')[-1].split('?')[0]
        token = chat_management.split('?t=')[-1]
        self.conversations[guest_jid] = (chat_id, token)

        self.log.debug('Adding conversation for {}: {}, {}'.format(guest_jid, chat_id, token))

    def introduce_myself(self, guest_jid):
        """
        Self-identify and let guests know what commands the bot knows.
        """
        commands = self.get_valid_commands()
        intro = 'Hi! I am a library bot and respond to the following commands:<br/>{}'.format(commands)
        self.send_message(mto = guest_jid, mbody = intro)

    def get_valid_commands(self):
        """
        Lets vistors know the commands a bot understands. If we allow
        for the possibility of transfer to a librarian, we need to make
        sure someone is around before getting a guest's hopes up.
        """
        valid_commands = '<br/>'

        for (command, (response, help_text)) in self.commands.items():
            valid_commands += '{} - {}<br/>'.format(command, help_text)

        if self.transfer_queue:
            result = requests.get('https://{}/presence/jid/{}/chat.{}/text'.format(self.domain, self.transfer_queue, self.domain))
            status = result.text
            if status == 'chat' or status == 'available':
                valid_commands += 'librarian - Chat with a real librarian instead'

        return valid_commands

    def process_command(self, msg):
        """
        Tries to figure out what a guests wants and delivers an
        answer or action the request to the best of the bot's ability.
        """
        user_request = msg['body'].lower()

        # Ignore system messages. The guest did not send them.
        if user_request.startswith('answered by'):
            return None
        if user_request.startswith('for transfers/etc see:'):
            return None
        if user_request.startswith('guest contacted '):
            return None
        if user_request.startswith('message not received'):
            return None
        if user_request.startswith('this is a transfer, please see:'):
            return None
        if user_request.startswith('transfer from'):
            return None
        if user_request.startswith('transferring... please wait'):
            return None
        if user_request.startswith('transferring to'):
            return None

        for (command, (response, _)) in self.commands.items():
            if command in user_request:
                return response

        if 'librarian' in user_request:
            if self.transfer(msg['from']):
                return None
            else:
                return 'Sorry, no librarians are available for chat.'

        return 'Sorry, I do not understand your request. Try one of these keywords: {}'.format(self.get_valid_commands())

    def transfer(self, guest_jid):
        """
        Attempts to transfer a guest to a librarian. The target of the
        transfer is a queue (presumably) staffed by librarians.
        """
        success = False

        if guest_jid.bare in self.conversations:
            conversation_id, token = self.conversations[guest_jid.bare]
            url = 'https://{}/2013-07-21/chats/{}/transfer'.format(self.domain, conversation_id)
            target = '{}@chat.{}'.format(self.transfer_queue, self.domain)

            self.log.debug('Transferring Guest to {}: {}'.format(target, url))
            result = requests.get(url, headers={'x-api-version': '2013-07-21'}, params = {'target': target, 't': token})
            success = result.status_code == 200

        return success

if __name__ == '__main__':
    # Setup the command line arguments.
    optp = OptionParser()

    # Output verbosity options.
    optp.add_option('-q', '--quiet', help='set logging to ERROR',
                    action='store_const', dest='loglevel',
                    const=logging.ERROR, default=logging.INFO)
    optp.add_option('-b', '--debug', help='set logging to DEBUG',
                    action='store_const', dest='loglevel',
                    const=logging.DEBUG, default=logging.INFO)
    optp.add_option('-v', '--verbose', help='set logging to COMM',
                    action='store_const', dest='loglevel',
                    const=5, default=logging.INFO)

    # bot options.
    optp.add_option('-u', '--username', dest='username',
                    help='username for your bot')
    optp.add_option('-p', '--password', dest='password',
                    help='password for your bot')
    optp.add_option('-t', '--transfer', dest='queue',
                    help='queue name to use for transfers to humans')
    optp.add_option('-d', '--domain', dest='domain',
                    help='chat domain')
    optp.add_option('-r', '--resource', dest='resource',
                    help='chat resource identifier')

    opts, args = optp.parse_args()

    # Setup logging.
    logging.basicConfig(level=opts.loglevel,
                        format='%(levelname)-8s %(message)s')
    log = logging.getLogger(__name__)

    if opts.username is None:
        opts.username = input('Bot Username: ')
    if opts.password is None:
        opts.password = getpass.getpass('Bot Password: ')
    if opts.queue is None:
        opts.queue = input('Transfer Queue: ')

    opts.domain = opts.domain or 'libraryh3lp.com'
    opts.resource = opts.resource or '84539179538333315078694562'

    # Setup the bot and register plugins. Note that while plugins may
    # have interdependencies, the order in which you register them does
    # not matter.
    xmpp = AnswerBot(opts.username, opts.domain, opts.resource, opts.password, log, opts.queue)
    xmpp.register_plugin('xep_0030') # Service Discovery
    xmpp.register_plugin('xep_0004') # Data Forms
    xmpp.register_plugin('xep_0054') # VCard Temp
    xmpp.register_plugin('xep_0060') # PubSub
    xmpp.register_plugin('xep_0085') # Chat state notifications
    xmpp.register_plugin('xep_0199') # XMPP Ping

    # LibraryH3lp only supports TLSv1.2
    xmpp.ssl_version = ssl.PROTOCOL_TLSv1_2

    # Connect to the XMPP server and start processing XMPP stanzas.
    #
    # If you do not have the dnspython library installed, you will need
    # to manually specify the name of the server if it does not match
    # the one in the JID. For example, to use Google Talk you would
    # need to use:
    #
    # if xmpp.connect(('talk.google.com', 5222)):
    #     ...
    xmpp.connect()
    xmpp.process(forever=True)
